<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class HomeController extends Controller {

     public function index() {

          $aboutMe = view('users.about_me');
          $awards = view('users.awards');
          $qualification = view('users.qualification');
          $skills = view('users.skills');
          $contact = view('users.contact');
          $portfolios = view('users.portfolio');


          return view('users.master')
                          ->with('aboutMe', $aboutMe)
                          ->with('skills', $skills)
                          ->with('qualification', $qualification)
                          ->with('portfolios', $portfolios)
                          ->with('awards', $awards)
                          ->with('contact', $contact);
     }

}
