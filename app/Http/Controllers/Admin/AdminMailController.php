<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminMailController extends AdminController
{
     public function __construct() {
          parent::__construct();
     }
     
     public function mailbox() {
          return view('admin.mail.mailbox');
     }

     public function compose() {
          return view('admin.mail.compose');
     }

     public function readMail() {
          return view('admin.mail.read_mail');
     }
}
