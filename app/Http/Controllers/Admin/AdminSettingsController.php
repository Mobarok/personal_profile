<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminSettingsController extends AdminController
{
    //
     public function __construct() {
          parent::__construct();
     }
     
     public function settings() {
          return view('admin.settings.settings');
     }
}
