<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{

    protected $table = "educations";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['institute', 'name', 'published', 'department','start_year','end_year'];

    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('published', 1);
    }
}
