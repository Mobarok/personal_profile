<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['designation', 'company_name', 'published', 'responsibility', 'join_date', 'leave_date'];

    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('published', 1);
    }

}
