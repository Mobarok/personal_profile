<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educations', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name',200);
            $table->string('institute',100);
            $table->string('department',150)->nullable();
<<<<<<< HEAD
            $table->date('start_year');
            $table->date('end_year');
=======
            $table->string('start_year', 4);
            $table->string('end_year', 4);
>>>>>>> 198ef5a4685ac65e385e048934c524fad5faa07d
            $table->boolean('published')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educations');
    }
}
