<?php

use Illuminate\Database\Seeder;
use App\Model\HireLink;
use Illuminate\Support\Facades\DB;

class HireLinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hire_links')->truncate();

        HireLink::create([
            'name' => 'PeoplePerHour',
            'logo' => '',
            'url' => 'http://www.pph.me/mobarok.hossen',
            'published' => 1
        ]);

        HireLink::create([
            'name' => 'Guru',
            'logo' => '',
            'url' => 'http://www.guru.com/freelancers/mobarok-hossen',
            'published' => 1
        ]);

        HireLink::create([
            'name' => 'Freelancer',
            'logo' => '',
            'url' => 'https://www.freelancer.com/u/mobarokhossen',
            'published' => 1
        ]);

        HireLink::create([
            'name' => 'Fiverr',
            'logo' => '',
            'url' => 'https://www.fiverr.com/mobarokraj',
            'published' => 1
        ]);

        HireLink::create([
            'name' => 'Truelancer',
            'logo' => '',
            'url' => 'https://www.truelancer.com/freelancer/mobarokhossen',
            'published' => 1
        ]);
    }
}
