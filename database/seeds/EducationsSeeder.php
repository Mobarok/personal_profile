<?php

use Illuminate\Database\Seeder;
use App\Model\Education;

class EducationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('educations')->truncate();

        Education::create([
            'name' => 'Masters of Science',
            'institute' => 'American International University - Bangladesh',
            'department' => 'Software Engineering',
            'start_year' => '2017',
            'end_year' => '2018',
            'published' => 1
        ]);

        Education::create([
            'name' => 'Bachelor of Science',
            'institute' => 'American International University - Bangladesh',
            'department' => 'Computer Science and Engineering',
            'start_year' => '2010',
            'end_year' => '2015',
            'published' => 1
        ]);

        Education::create([
            'name' => 'Higher Secondary School Certificate',
            'institute' => 'Mern Sun College',
            'department' => 'Science',
            'start_year' => '2007',
            'end_year' => '2009',
            'published' => 1
        ]);

        Education::create([
            'name' => 'Secondary School Certificate',
            'institute' => 'Mirsarai Model Pilot High School',
            'department' => 'Science',
            'start_year' => '-',
            'end_year' => '2007',
            'published' => 1
        ]);
    }
}
