<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Model\ProjectCategory;

class ProjectCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project_categories')->truncate();

        $array = [
            'Web Application' => 'web_application',
            'Web Development' => 'web_development',
            'Web Design' => 'web_design',
            'Web Customization' => 'web_customize',
            'E-Commerce' => 'e-commerce',
        ];

        foreach ($array as $key => $item) {
            ProjectCategory::create([
                'project_categories_name' => $key,
                'filter' => $item,
                'published' => 1
            ]);
        }


    }
}
