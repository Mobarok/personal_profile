<?php

use Illuminate\Database\Seeder;
use App\Model\Employment;
use Carbon\Carbon;

class EmploymentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employments')->truncate();

        Employment::create([
            'designation' => 'Software Engineer',
            'company_name' => 'TikTok Bangladesh',
            'join_date' =>  Carbon::create(2017, 1, 3)->toDateString(),
            'leave_date' => null,
            'published' => 1
        ]);

        Employment::create([
            'designation' => 'Laravel Developer',
            'company_name' => 'Informatix Technologies',
            'join_date' => Carbon::create(2016, 10, 1)->toDateString(),
            'leave_date' => Carbon::create(2016, 12, 31)->toDateString(),
            'published' => 1
        ]);

        Employment::create([
            'designation' => 'Web Developer',
            'company_name' => 'Freelancer',
            'join_date' => Carbon::create(2015, 6, 10)->toDateString(),
            'leave_date' => null,
            'published' => 1
        ]);

        Employment::create([
            'designation' => 'Internship',
            'company_name' => 'Workspace Infotech',
            'join_date' => Carbon::create(2014, 9, 8)->toDateString(),
            'leave_date' => Carbon::create(2014, 12, 10)->toDateString(),
            'published' => 1
        ]);
    }
}
