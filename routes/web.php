<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');
Route::put('/contact', 'ContactController@saveMessage');

Auth::routes();


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {

     Route::get('/home', 'AdminController@index');
     Route::get('/mailbox', 'AdminMailController@mailbox');
     Route::get('/compose', 'AdminMailController@compose');
     Route::get('/read-mail', 'AdminMailController@readMail');
     Route::get('/settings', 'AdminSettingsController@settings');
     Route::get('/education', 'AdminEducationController@getEducation');
});

