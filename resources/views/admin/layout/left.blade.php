@extends('admin.layout.header')
@section('left')

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
     <!-- sidebar: style can be found in sidebar.less -->
     <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
               <div class="pull-left image">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
               </div>
               <div class="pull-left info">
                    <p>{{auth::User()->name}}</p>
                    <a href="{{URL::to('/admin/home')}}"><i class="fa fa-circle text-success"></i> Online</a>
               </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
               <li class="active treeview">
                    <a href="{{URL::to('admin/home')}}">
                         <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
               </li>
                <li>
                    <a href="{{URL::to('admin/settings')}}">
                         <i class="fa fa-cog"></i> <span>Settings</span>
                    </a>
               </li>
               <li>
                    <a href="{{URL::to('/admin/')}}">
                         <i class="fa fa-language"></i> <span>Skills</span>
                    </a>
               </li>
               <li>
                    <a href="{{URL::to('/admin/')}}">
                         <i class="fa fa-graduation-cap"></i> <span>Education</span>
                    </a>
               </li>
               <li>
                    <a href="{{URL::to('/admin/')}}">
                         <i class="fa fa-briefcase"></i> <span>Experience</span>
                    </a>
               </li>
               <li>
                    <a href="{{URL::to('/admin/')}}">
                         <i class="fa fa-book"></i> <span>Training</span>
                    </a>
               </li>
               <li>
                    <a href="{{URL::to('/admin/')}}">
                         <i class="fa fa-image"></i> <span>Portfolios</span>
                    </a>
               </li> 
               <li>
                    <a href="{{URL::to('/admin/')}}">
                         <i class="fa fa-th-list"></i> <span>Project Category</span>
                    </a>
               </li>
              
               <li>
                    <a href="{{URL::to('admin/mailbox')}}">
                         <i class="fa fa-envelope"></i> <span>Mailbox</span>
                         <span class="pull-right-container">
                              <small class="label pull-right bg-yellow">12</small>
                              <small class="label pull-right bg-green">16</small>
                              <small class="label pull-right bg-red">5</small>
                         </span>
                    </a>
               </li>
               <li class="treeview">
                    <a href="{{URL::to('/admin/')}}">
                         <i class="fa fa-files-o"></i>
                         <span>Blogs</span>
                         <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                         </span>
                    </a>
                    <ul class="treeview-menu">
                         <li><a href="{{URL::to('/admin/')}}"><i class="fa fa-plus-circle"></i> Add Blogs</a></li>
                         <li><a href="{{URL::to('/admin/')}}"><i class="fa fa-circle-o"></i>Manage Blogs</a></li>
                         <li><a href="{{URL::to('/admin/')}}"><i class="fa fa-circle-o"></i>Pending Blogs</a></li>
                    </ul>
               </li>
          </ul>
     </section>
     <!-- /.sidebar -->
</aside>

@yield('content')

@endsection