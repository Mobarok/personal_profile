@extends('admin.layout.left')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
          <h1>
               Dashboard
               <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
               <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
               <li class="active">Dashboard</li>
          </ol>
     </section>

     <!-- Main content -->
     <section class="content">
          <div class="row">
               <div class="col-xs-12">
                    <div class="box">
                         <div class="box-header">
                              <h3 class="box-title">Manage</h3>
                         </div>
                         <!-- /.box-header -->
                         <div class="box-body table-responsive no-padding">
                              <table class="table table-hover">
                                   <tr>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Reason</th>
                                   </tr>
                                   <tr>
                                        <td>183</td>
                                        <td>John Doe</td>
                                        <td>11-7-2014</td>
                                        <td><span class="label label-success">Approved</span></td>
                                        <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                                   </tr>
                              </table>
                         </div>
                         <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
               </div>
          </div>
          <!-- /.row -->
     </section>
     <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection