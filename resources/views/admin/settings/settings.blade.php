@extends('admin.layout.left')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
          <h1>
               Settings
               <small>Admin Panel</small>
          </h1>
          <ol class="breadcrumb">
               <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
               <li class="active">Settings</li>
          </ol>
     </section>

     <!-- Main content -->
     <section class="content">
          <!-- Small boxes (Stat box) -->

          <div class="row">

               <div class="col-md-5">

                    <div class="box box-default collapsed-box">
                         <div class="box-header with-border">
                              <h3 class="box-title">Personal Information</h3>

                              <div class="box-tools pull-right">
                                   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                   </button>
                              </div>
                              <!-- /.box-tools -->
                         </div>
                         <!-- /.box-header -->
                         {!! Form::open(['url'=>'admin/personal-info/', 'method'=>'post']) !!}
                         <div class="box-body">
                              <div class="form-group">
                                   <label for="name">Name</label>
<<<<<<< HEAD
=======

>>>>>>> 198ef5a4685ac65e385e048934c524fad5faa07d
                                   <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
                              </div> 
                              <div class="form-group">
                                   <label for="position">Position</label>
                                   <input type="text" class="form-control" id="position" name="position" placeholder="Enter position">
                              </div>
                              <div class="form-group">
                                   <label>About me</label>
                                   <textarea class="form-control" rows="3" name="about_me" placeholder="about me" width="100%" ></textarea>
                              </div>
                              <!-- /.box-body -->
                         </div>
                         <div class="box-footer">
                              <button type="submit" class="btn btn-primary pull-right">Submit</button>
                         </div>
                         {!!  Form::close() !!}
                    </div>

                    <div class="box box-default collapsed-box">
                         <div class="box-header with-border">
                              <h3 class="box-title">AWARDS & ACHIEVEMENTS</h3>

                              <div class="box-tools pull-right">
                                   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                   </button>
                              </div>
                              <!-- /.box-tools -->
                         </div>
<<<<<<< HEAD
=======

>>>>>>> 198ef5a4685ac65e385e048934c524fad5faa07d
                         {!! Form::open(['url'=>'admin/achievement/', 'method'=>'post']) !!}
                         <div class="box-body">
                              <div class="form-group">
                                   <label for="award_win">Awards Win</label>
                                   <input type="number" class="form-control" id="award_win" name="awards_win" placeholder="Enter Awards Win">
                              </div>
                              <div class="form-group">
                                   <label for="project">Projects Done</label>
                                   <input type="number" class="form-control" id="project" name="projects" placeholder="Enter Projects Done">
                              </div>
                              <div class="form-group">
                                   <label for="clients">Happy Clients</label>
                                   <input type="number" class="form-control" id="clients" name="clients" placeholder="Enter Happy Clients">
                              </div>
                              <div class="form-group">
                                   <label for="coffee">Cups of Coffee</label>
                                   <input type="number" class="form-control" id="coffee" name="cup_of_coffee" placeholder="Enter Cup of Coffee">
<<<<<<< HEAD
=======

>>>>>>> 198ef5a4685ac65e385e048934c524fad5faa07d
                              </div>
                              <!-- /.box-body -->
                         </div>
                         <div class="box-footer">
                              <button type="submit" class="btn btn-primary pull-right">Submit</button>
                         </div>
                         {!!  Form::close() !!}
                         <!-- /.box-body -->
                    </div>

                    <div class="box box-default collapsed-box">
                         <div class="box-header with-border">
                              <h3 class="box-title">Resume</h3>

                              <div class="box-tools pull-right">
                                   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                   </button>
                              </div>
                              <!-- /.box-tools -->
                         </div>
                         <!-- /.box-header -->
<<<<<<< HEAD
                         {!! Form::open(['url'=>'admin/resume/', 'method'=>'post']) !!}
=======

                         {!! Form::open(['url'=>'admin/resume/', 'method'=>'post']) !!}

>>>>>>> 198ef5a4685ac65e385e048934c524fad5faa07d
                         <div class="box-body">
                              <div class="form-group">
                                   <label for="resume">Resume</label>
                                   <input type="file"  id="resume">
                              </div>
                         </div>
                         <div class="box-footer">
                              <button type="submit" class="btn btn-primary pull-right">Submit</button>
                         </div>
                         {!!  Form::close() !!}
                    </div>

                    <div class="box box-default collapsed-box">
                         <div class="box-header with-border">
<<<<<<< HEAD
                              <h3 class="box-title">My Skills</h3>

=======

                              <h3 class="box-title">My Skills</h3>


>>>>>>> 198ef5a4685ac65e385e048934c524fad5faa07d
                              <div class="box-tools pull-right">
                                   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                   </button>
                              </div>
                              <!-- /.box-tools -->
                         </div>
                         <!-- /.box-header -->
                         {!! Form::open(['url'=>'admin/personal-info/', 'method'=>'post']) !!}
                         <div class="box-body">
                              <div class="form-group">
                                   <label for="name">Skill Description</label>
<<<<<<< HEAD
                                   <textarea class="form-control" rows="3" name="skill_desc" placeholder="Skill Description" width="100%" ></textarea>
=======
                                   <textarea class="form-control" rows="3" name="skill_desc"
>>>>>>> 198ef5a4685ac65e385e048934c524fad5faa07d
                              </div>
                              <!-- /.box-body -->
                         </div>
                         <div class="box-footer">
                              <button type="submit" class="btn btn-primary pull-right">Submit</button>
                         </div>
                         {!!  Form::close() !!}
                         <!-- /.box-body -->
                    </div>
               </div>
               <div class="col-md-5">
                    <div class="box box-default collapsed-box">
                         <div class="box-header with-border">
                              <h3 class="box-title">Social Media</h3>

                              <div class="box-tools pull-right">
                                   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                   </button>
                              </div>
                              <!-- /.box-tools -->
                         </div>
                         <!-- /.box-header -->
                         {!! Form::open(['url'=>'admin/personal-social-link/', 'method'=>'post']) !!}
                         <div class="box-body">
                              <div class="form-group">
                                   <label for="facebook">Facebook</label>
                                   <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Facebook Name">
                              </div>
                              <div class="form-group">
                                   <label for="bitbucket">Bit-Bucket</label>
                                   <input type="text" class="form-control" id="bitbucket" name="bitbucket" placeholder="Bit bucket Username">
                              </div>
                              <div class="form-group">
                                   <label for="github">Github</label>
                                   <input type="text" class="form-control" id="github" name="github" placeholder="Github Username">
                              </div>
                              <div class="form-group">
                                   <label for="linkedin">Linkedin</label>
                                   <input type="text" class="form-control" id="linkedin" name="linkedin" placeholder="Enter Name">
                              </div>
                              <div class="form-group">
                                   <label for="twitter">Twitter</label>
                                   <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Twitter Username">
                              </div>
                              <div class="form-group">
                                   <label for="google">Google+</label>
                                   <input type="text" class="form-control" id="google" name="google" placeholder="Google account email">
                              </div>
                              <div class="form-group">
                                   <label for="skype">Skype</label>
                                   <input type="text" class="form-control" id="skype" name="skype" placeholder="Skype username">
                              </div>
                              <!-- /.box-body -->
                         </div>
                         <div class="box-footer">
                              <button type="submit" class="btn btn-primary pull-right">Submit</button>
                         </div>
                         {!!  Form::close() !!}
                         <!-- /.box-body -->
                    </div>

                    <div class="box box-default collapsed-box">
                         <div class="box-header with-border">
                              <h3 class="box-title">Contacts</h3>

                              <div class="box-tools pull-right">
                                   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                   </button>
                              </div>
                              <!-- /.box-tools -->
                         </div>
                         {!! Form::open(['url'=>'admin/personal-contact/', 'method'=>'post']) !!}
                         <div class="box-body">
                              <div class="form-group">
                                   <label for="phone">Phone</label>
                                   <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter Phone Number">
                              </div>
                              <div class="form-group">
                                   <label for="email">Email</label>
                                   <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email">
                              </div>
                              <div class="form-group">
                                   <label for="address">Address</label>
                                   <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">
                              </div>
                              <div class="form-group">
                                   <label for="city">City</label>
                                   <input type="text" class="form-control" id="city" name="city" placeholder="Enter City">
                              </div>
                              <div class="form-group">
                                   <label for="country">Country</label>
                                   <input type="text" class="form-control" id="country" name="country" placeholder="Enter Country">
                              </div>
                              <!-- /.box-body -->
                         </div>
                         <div class="box-footer">
                              <button type="submit" class="btn btn-primary pull-right">Submit</button>
                         </div>
                         {!!  Form::close() !!}
                    </div>


                    <!-- /.box -->
               </div>
          </div>

     </section>
</div>
@endsection