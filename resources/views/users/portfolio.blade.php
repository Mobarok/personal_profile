@extends('users.master')
@section('portfolios')
<section class="main-section paddind" id="Portfolio"><!--main-section-start-->
     <div class="container">
          <h2>Portfolio</h2>
          <h6>My portfolios with live link which make me good Developer</h6>
          <div class="portfolioFilter">  
               <ul class="Portfolio-nav wow fadeIn delay-02s">
                    <li><a href="#" data-filter="*" class="current" >All</a></li>
                    <li><a href="#" data-filter=".webdevelopment" >Web Development</a></li>
                    <li><a href="#" data-filter=".webdesign" >Web design</a></li>
                    <li><a href="#" data-filter=".customization" >Custom Web Application</a></li>
                    <li><a href="#" data-filter=".ecommerce" >E-Commerce</a></li>
                    <li><a href="#" data-filter=".reswebdesign" >Responsive Web Design</a></li>

               </ul>
          </div> 

     </div>
     <div class="portfolioContainer wow fadeInUp delay-04s">

        <div class=" Portfolio-box webdesign reswebdesign">
            <div class="img-link">
                <a href="http://www.tiktok.mobarokhossen.me" target="_blank"><img src="{{URL::to('portfolios/tiktok-responsive.png')}}" alt=" TikTok Web design by Mobarok Hossen"></a>
            </div>
            <h3>TikTok</h3>
            <p> Web Design</p>
        </div>
         <div class=" Portfolio-box webdesign reswebdesign webdevelopment">
             <div class="img-link">
                 <a href="http://www.accounting.mobarokhossen.me" target="_blank"><img src="{{URL::to('portfolios/accounting-software.png')}}" alt="Accounting System work by Mobarok Hossen"></a>
             </div>
             <h3>Accounting System</h3>
             <p> Web Development</p>
         </div>
         <div class=" Portfolio-box webdesign reswebdesign webdevelopment">
             <div class="img-link">
                 <a href="http://www.invoice.mobarokhossen.me" target="_blank"><img src="{{URL::to('portfolios/invoicing-system.png')}}" alt="Invoicing System work by Mobarok Hossen"></a>
             </div>
             <h3>Invoicing System</h3>
             <p> Web Development</p>
         </div>
        <div class=" Portfolio-box webdevelopment reswebdesign">
            <div class="img-link">
                <a href="http://www.komblot.informatixbd.com" target="_blank"><img src="{{URL::to('portfolios/KomBlot-work.png')}}" alt="KomBlot work by Mobarok Hossen"></a>
            </div>
            <h3>Komblot</h3>
            <p> Web Development using Laravel</p>
        </div>
        <div class=" Portfolio-box webdevelopment">
            <div class="img-link">
                <a href="http://www.patientcare.mobarokhossen.me" target="_blank"><img src="{{URL::to('portfolios/patient-care.png')}}" alt="Patient Care Developed by Mobarok Hossen"></a>
            </div>
            <h3>Patient Care</h3>
            <p>Web development using Laravel</p>
        </div>
        <div class=" Portfolio-box webdesign reswebdesign" >
            <div class="img-link">
                <a href="http://www.miadw.com/my_profile" target="_blank"><img src="{{URL::to('portfolios/Mobarok Hossen.png')}}" alt=""></a>
            </div>
            <h3>Web Design</h3>
            <p>PSD to HTML, Web Design</p>
        </div>
        <div class="Portfolio-box customization">
            <div class="img-link">
                <a href="https://www.globalentrytest.org.pk/" target="_blank" ><img src="{{URL::to('portfolios/online_test.png')}}" alt=""></a>
            </div>
            <h3>Global Test (Online Exam System)</h3>
            <p>Web Customization</p>
        </div>
        <div class=" Portfolio-box webdesign reswebdesign">
            <div class="img-link">
                <a href="http://www.deluxelodge.mobarokhossen.me" target="_blank"><img src="{{URL::to('portfolios/deluxe_lodge.png')}}" alt=""></a>
            </div>
            <h3>Deluxe Lodge</h3>
            <p>PSD to HTML, Web Design</p>
        </div>
        <div class="Portfolio-box customization">
            <div class="img-link">
                <a href="http://www.capitalforyourbusiness.com" target="_blank" ><img src="{{URL::to('portfolios/finance_custom.png')}}" alt=""></a>
            </div>
            <h3>www.capitalforyourbusiness.com</h3>
            <p>Web Customization</p>
        </div>
     </div>
</section>
@stop