@extends('users.master')
@section('contact')
<div class="container">
     <section class="main-section contact" id="contact">
          <h2>Contact Me</h2>
          <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-12 contact-address col-xs-12 wow fadeInLeft">
                    <div class="contact-info-box address clearfix">
                         <h3><i class=" icon-map-marker"></i>Address:</h3>
                         <span>Nikunja-2, Khilkhet, Dhaka-1229</span>
                    </div>
                    <div class="contact-info-box phone clearfix">
                         <h3><i class="fa-phone"></i>Phone:</h3>
                         <span>+880-1816-518102</span>
                    </div>
                    <div class="contact-info-box email clearfix">
                         <h3><i class="fa-pencil"></i>email:</h3>
                         <span>mobarok_hossen@outlook.com</span>
                    </div>
                    <div class="contact-info-box hours clearfix">
                         <h3><i class="fa-clock-o"></i>Hours:</h3>
                         <span><strong>Tuesday - Saturday:</strong> 10am - 6pm<br><strong>Monday - Sunday : </strong> Best not to ask.</span>
                    </div>
                    <ul class="social-link">


                         <li class="twitter"><a href="https://bd.linkedin.com/in/mobarokhossen" target="_blank"><i class="fa-linkedin"></i></a></li>
                         <li class="github"><a href="https://github.com/mobarokhossen" target="_blank"><i class="fa-github"></i></a></li>
                         <li class="bitbucket"><a href="https://bitbucket.org/Mobarok" target="_blank"><i class="fa-bitbucket"></i></a></li>
                         <li class="twitter"><a href="https://twitter.com/Mobarokraj" target="_blank"><i class="fa-twitter"></i></a></li>
                         <li class="facebook"><a href="https://www.facebook.com/hossenmobarok" target="_blank"><i class="fa-facebook"></i></a></li>
                         <!--<li class="skype"><a href="#" target="_blank"><i class="fa-skype"></i></a></li>-->
                    </ul>
                    <div>
                         <ul class="hire-link">
                              <li class="btn btn-primary ">Hire Me <i class="fa fa-arrow-circle-right"></i> </li>
                              <li ><a href="http://www.pph.me/mobarok.hossen" target="_blank"><img src="img/PeoplePerHour.jpg" class="img-rounded"></a></li>
                              <li ><a href="http://www.guru.com/freelancers/mobarok-hossen" target="_blank"><img src="img/guru.jpg" class="img-rounded"></a></li>
                              <!--<li ><a href="" target="_blank"><img src="img/upwork.png" class="img-rounded"></a></li>-->
                              <li ><a href="https://www.freelancer.com/u/mobarokhossen.html" target="_blank"><img src="img/freelancer.png" class="img-rounded"></a></li>
                              <li ><a href="https://www.fiverr.com/mobarokraj" target="_blank"><img src="img/fiverr.png" class="img-rounded"></a></li>
                              <li ><a href="https://www.truelancer.com/freelancer/mobarokhossen" target="_blank"><img src="img/truelancer.png" class="img-rounded"></a></li>
                         </ul>
                    </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-10 col-xs-10 contact-form  wow fadeInUp delay-05s">
                    <div class="form">
                         
                      {!! Form::open(['url' => '/contact', 'method' => 'PUT' ]) !!}
                         <input class="input-text" type="text" name="" placeholder="Your Name *" onfocus="if (this.value == this.defaultValue)
                                        this.value = '';" onblur="if (this.value == '')
                                                       this.value = this.defaultValue;">
                         <input class="input-text" type="email" name="" placeholder="Your E-mail *" onfocus="if (this.value == this.defaultValue)
                                        this.value = '';" onblur="if (this.value == '')
                                                       this.value = this.defaultValue;">
                         <input class="input-text" type="text" name="" placeholder="Subject *" onfocus="if (this.value == this.defaultValue)
                                        this.value = '';" onblur="if (this.value == '')
                                                       this.value = this.defaultValue;">
                         <textarea class="input-text text-area" cols="0" rows="0" onfocus="if (this.value == this.defaultValue)
                                        this.value = '';" onblur="if (this.value == '')
                                                       this.value = this.defaultValue;">Your Message *</textarea>
                         <input class="input-btn" type="submit" value="send message">
                         
                        {!! Form::close() !!}
                    </div>
               </div>
          </div>
     </section>
</div>
@stop
