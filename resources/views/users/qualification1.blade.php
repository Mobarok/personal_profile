@extends('users.master')
@section('qualification')
          <!--- Experience Starts Here --->
          <section id="qualification" class="main-section experience">
               <div class="container">
                    <div class="exp-top">
                         <h2 >Qualification</h2>
                         <span class="line"></span>
                    </div>
                    <div class="exp-devide">
                         <h4>Work Experience</h4>
                         <span class="devide-line">
                         </span>
                         <div class="label"><i class="fa fa-briefcase text-primary" aria-hidden="true"></i></div>
                          <div class="exp-devide-grid-right">
                              <h5>Laravel Developer</h5>
                              <small>Informatix Technologies,Oct 2016 - Present</small>
                              <p></p>
                         </div>
                         <div class="exp-devide-grid-left">
                              <h5>Web Developer</h5>
                              <small>Freelancer, 2015-Present</small>
                              <p></p>
                         </div>
                         <div class="exp-devide-grid-right">
                              <h5>Front End Developer</h5>
                              <small>Freelancer, 2015-present</small>
<!--                              <p>Responsible for designing and coding the website. Responsive web design and use slide show. cross browsing compatibility. Duty for testing the code.</p>-->
                         </div>
                         <div class="exp-devide-grid-left">
                              <h5>Internship (JUNIOR Web Developer)</h5>
                              <small>Workspace Infotech, 2014</small>
<!--                              <p>Building PHP websites using PHP based frameworks. Back end development and maintenance of websites using PHP and MySQL. Developing compatible User Interface functionality using jQuery & other libraries. Developing web sites using MySQL, PHP,  HTML & other programming tools. Ongoing design and maintenance of new and existing websites. Interacting with designers, programmers and clients. Designing websites that are easy and effective to use. Testing websites for functionality in different browsers & at different resolutions. </p>-->
                         </div>
                    </div>
                    <div class="exp-devide education">
                         <h4>Education</h4>
                         <span class="devide-line">
                         </span>

                         <div class="label graduation"><i class="fa fa-graduation-cap text-primary" aria-hidden="true"></i></div>
                         <div class="exp-devide-grid-right">
                              <h5>BACHELOR OF SCIENCE</h5>
                              <small>American International University- Bangladesh  2010-2015</small>
                              <p>Computer science and engineering</p>
                         </div>
                         <div class="exp-devide-grid-left">
                              <h5>HIGHER SECONDARY SCHOOL CERTIFICATE </h5>
                              <small>Mern Sun College  2007-2009</small>
                              <p></p>
                         </div>
                         <div class="exp-devide-grid-right">
                              <h5>SECONDARY SCHOOL CERTIFICATE </h5>
                              <small>Mirsarai Pilot high School  2007-2009</small>
                              <p></p>
                         </div>
                    </div>
                    <div class="exp-devide training">
                         <h4>Training / Certification</h4>
                         <span class="devide-line">
                         </span>
                         <div class="label "> <i class="fa fa-book text-primary" aria-hidden="true"></i></div>
                         <div class="exp-devide-grid-right">
                              <h5>Advance PHP, MySQL & Laravel Framework</h5>
                              <small>Bdjobs.com & LICT program,   Duration: 184 Hours</small>
                              <p>Dhaka, Bangladesh</p>
                         </div>
                         <div class="exp-devide-grid-left">
                              <h5>Advance PHP & MySQL </h5>
                              <small>Techmasters,  Duration: 5 Months</small>
                              <p></p>
                         </div>
                         <div class="exp-devide-grid-right">
                              <h5>Introduction to Scrum</h5>
                              <small>Techmasters,  8 Hours</small>
                              <p></p>
                         </div>
                    </div>
               </div>
          </section>
          <!--- Experience Ends Here --->

@stop