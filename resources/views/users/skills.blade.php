@extends('users.master')
@section('skills')
<div class="skills main-section" id="skills">
    <div class="container">
        <h2>My Skills</h2>
        <label class="line"></label>
        <div class="col-md-6 skills-right">
            <div class="bar_group">
                <div class='bar_group__bar thin' label='HTML, CSS, Bootstrap' show_values='true' tooltip='true' value='83'></div>

                <div class='bar_group__bar thin' label='Javascript, JQuery' show_values='true' tooltip='true' value='60'></div>
                <div class='bar_group__bar thin' label='Photoshop, Illustrator' show_values='true' tooltip='true' value='50'></div>
                <div class='bar_group__bar thin' label='Wordpress' show_values='true' tooltip='true' value='45'></div>
            </div>
        </div>
        <div class="col-md-6 skills-right">

                <div class='bar_group__bar thin' label='PHP' show_values='true' tooltip='true' value='70'></div>

                <div class='bar_group__bar thin' label='Laravel' show_values='true' tooltip='true' value='60'></div>

                <div class='bar_group__bar thin' label='MySQL, Oracle SQL' show_values='true' tooltip='true' value='65'></div>
            </div>
            <script src="js/bars.js"></script>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
@stop