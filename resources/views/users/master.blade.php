<!doctype html>
<html>
     <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, maximum-scale=1">

          <title>Mobarok Hossen | Professional Web Developer</title>
          <link rel="icon" href="favicon.png" type="image/png">
          <link rel="shortcut icon" href="favicon.ico" type="img/x-icon">

          <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
          <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>

          <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
          <link href="css/style.css" rel="stylesheet" type="text/css">
          <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
          <link href="css/responsive.css" rel="stylesheet" type="text/css">
          <link href="css/animate.css" rel="stylesheet" type="text/css">

          <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->

          <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
          <script type="text/javascript" src="js/bootstrap.js"></script>
          <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
          <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
          <script type="text/javascript" src="js/jquery.isotope.js"></script>
          <script type="text/javascript" src="js/wow.js"></script>
          <script type="text/javascript" src="js/classie.js"></script>

          <!--[if lt IE 9]>
              <script src="js/respond-1.1.0.min.js"></script>
              <script src="js/html5shiv.js"></script>
              <script src="js/html5element.js"></script>
          <![endif]-->


     </head>
     <body>

          <section id="home" class="fill">
               <div class="home-background parallax-section">
                    <div class="container-fluid">
                         <div class="row">
                              <div class="home-box col-xs-12">
                                   <div>
                                        <img src="{{URL::to('img/avatar.jpg')}}" alt="">
                                        <h1>Mobarok Hossen</h1>
                                        <p>Laravel Developer / Web Developer</p>
                                   </div>
                                   <div class="social">
                                        <ul>
                                             <li><a href="https://bd.linkedin.com/in/mobarokhossen" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                             <li><a href="https://github.com/mobarokhossen" target="_blank"><i class="fa fa-github"></i></a></li>
                                             <li><a href="https://bitbucket.org/Mobarok" target="_blank"><i class="fa fa-bitbucket"></i></a></li>
<!--                                             <li><a href="#" target="_blank"><i class="fa fa-skype"></i></a></li>-->
                                             <li><a href="https://www.facebook.com/hossenmobarok" target="_blank"> <i class="fa fa-facebook"></i></a></li>
                                             <li><a href="https://twitter.com/Mobarokraj" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                        </ul>
                                   </div>
                              </div>
                         </div><!-- end row -->
                    </div><!-- end container -->
               </div>
          </section>



          <nav class="main-nav-outer" id="test"><!--main-nav-start-->
               <div class="container-fluid">
                    <ul class="main-nav">
                         <li><a href="#home">Home</a></li>
                         <li><a href="#about">About</a></li>
                         <li><a href="#qualification">Qualification</a></li>
                         <li class="small-logo"><a href="#home"><img src="{{URL::to('img/avatar.jpg')}}" alt="" class="img-circle" width="80px"></a></li>
                         
                         <li><a href="#skills">Skills</a></li>
                         <li><a href="#Portfolio">Portfolio</a></li>
                         <li><a href="#contact">Contact</a></li>
                         <!--<li><a href="#team">Blogs</a></li>-->
                    </ul>
                   
                   <a class="mobile-logo" href="#home" ><img src="{{URL::to('img/avatar.jpg')}}" alt="" class="img-circle" width="80px"></a>
                    <a class="res-nav_click" href="#"><i class="fa-bars"></i></a>
               </div>
          </nav><!--main-nav-end-->



          <!-- About Section
       ================================================== -->
        @yield('about-me')
          <!-- About Section End-->

          <!--skills-->
          @yield('skills')
          <!--/skills-->
          
          
          <!--- Experience Starts Here --->
          @yield('qualification')
          <!--- Experience Ends Here --->


         <!--- portfolio Starts Here --->
          @yield('portfolios')
          <!--- portfolio Ends Here --->
          
                   <!--- awards Starts Here --->
          @yield('awards')
          <!--- awards Ends Here --->
                      
         <!--- contact Starts Here --->
          @yield('contact')
          <!--- contact Ends Here --->
          
          
          <footer class="footer">
               <div class="container">
                    <span class="copyright">Copyright © 2016 |  Mobarok Hossen</span>
               </div>
          </footer>


          <script type="text/javascript">
               $(document).ready(function (e) {
                    $('#test').scrollToFixed();
                    $('.res-nav_click').click(function () {
                         $('.main-nav').slideToggle();
                         return false

                    });

               });
          </script>

          <script>
               wow = new WOW(
                       {
                            animateClass: 'animated',
                            offset: 100
                       }
               );
               wow.init();

          </script>


          <script type="text/javascript">
               $(window).load(function () {

                    $('.main-nav li a').bind('click', function (event) {
                         var $anchor = $(this);

                         $('html, body').stop().animate({
                              scrollTop: $($anchor.attr('href')).offset().top - 102
                         }, 1500, 'easeInOutExpo');
                         /*
                          if you don't want to use the easing effects:
                          $('html, body').stop().animate({
                          scrollTop: $($anchor.attr('href')).offset().top
                          }, 1000);
                          */
                         event.preventDefault();
                    });
               })
          </script>

          <script type="text/javascript">

               $(window).load(function () {


                    var $container = $('.portfolioContainer'),
                            $body = $('body'),
                            colW = 375,
                            columns = null;


                    $container.isotope({
                         // disable window resizing
                         resizable: true,
                         masonry: {
                              columnWidth: colW
                         }
                    });

                    $(window).smartresize(function () {
                         // check if columns has changed
                         var currentColumns = Math.floor(($body.width() - 30) / colW);
                         if (currentColumns !== columns) {
                              // set new column count
                              columns = currentColumns;
                              // apply width to container manually, then trigger relayout
                              $container.width(columns * colW)
                                      .isotope('reLayout');
                         }

                    }).smartresize(); // trigger resize to set container width
                    $('.portfolioFilter a').click(function () {
                         $('.portfolioFilter .current').removeClass('current');
                         $(this).addClass('current');

                         var selector = $(this).attr('data-filter');
                         $container.isotope({
                              filter: selector,
                         });
                         return false;
                    });

               });

          </script>
     </body>
</html>

