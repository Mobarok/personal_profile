@extends('users.master')
@section('awards')
<section >
     <!--- Awards Starts Here ---->
     <div class="awards">
          <div class="container">
               <div class="awards-top">
                    <h2>AWARDS & ACHIEVEMENTS</h2>
                    <div class="row award-row">
                         <div class="col-md-pull-3 award-column">
                              <i class="award"></i>
                              <h5>10</h5>
                              <small>Awards Won!</small>
                         </div>
                         <div class="col-md-pull-3 award-column">
                              <i class="project"></i>
                              <h5>20</h5>
                              <small>PROJECTS DONE</small>
                         </div>
                         <div class="col-md-pull-3 award-column">
                              <i class="client"></i>
                              <h5>10</h5>
                              <small>HAPPY CLIENTS</small>
                         </div>
                         <div class="col-md-pull-3 award-column">
                              <i class="cups"></i>
                              <h5>580</h5>
                              <small>CUPS OF COFFEE</small>
                         </div>
                         <div class="clearfix"> </div>
                    </div>
               </div>
          </div>
     </div>
     <!--- Awards Ends Here ---->
</section>
@stop