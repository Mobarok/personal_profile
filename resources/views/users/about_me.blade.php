@extends('users.master')
@section('about-me')
<section id="about" class="main-section">
     <div class="container">
          <h2>About Me</h2>
          <p>
              I'm a professional software engineer. I have more than 1 year experience in Web development. I have strong knowledge in web programming.
            <br/><br/>

              A resourceful and innovative PHP developer with extensive experience in the layout, design and coding of websites specifically in PHP format. Possessing considerable knowledge of the development of web applications and scripts using PHP programming language and MySQL & SQL Server databases.
          <br/>

          Looking for a suitable engineer position to join a dynamic, ambitious, growing company and forge a career as a software engineer. <br/><br/>
          <q > Inquisitive to create something which useful for people </q>

          </p>
          
          <div class="row">
               <div class="columns download">
                    <p>
                         <a href="{{URL::to('/resume/Web_Developer_Mobarok_Hossen.pdf')}}" class="button"><i class="fa fa-download"></i>Download Resume</a>
                    </p>
               </div>

          </div> <!-- end row -->

     </div> <!-- end .main-col -->
</section> 

@stop