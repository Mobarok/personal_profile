<!doctype html>
<html>
     <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, maximum-scale=1">
          <meta name="p:domain_verify" content="687a13f515f2e08f72ec4ebe636f322a"/>


          <title>Mobarok Hossen | Professional Web Developer</title>
          <link rel="icon" href="favicon.png" type="image/png">
          <link rel="shortcut icon" href="favicon.ico" type="img/x-icon">

          <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
          <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>

          <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
          <link href="css/style.css" rel="stylesheet" type="text/css">
          <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
          <link href="css/responsive.css" rel="stylesheet" type="text/css">
          <link href="css/animate.css" rel="stylesheet" type="text/css">

          <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->

          <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
          <script type="text/javascript" src="js/bootstrap.js"></script>
          <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
          <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
          <script type="text/javascript" src="js/jquery.isotope.js"></script>
          <script type="text/javascript" src="js/wow.js"></script>
          <script type="text/javascript" src="js/classie.js"></script>

          <!--[if lt IE 9]>
              <script src="js/respond-1.1.0.min.js"></script>
              <script src="js/html5shiv.js"></script>
              <script src="js/html5element.js"></script>
          <![endif]-->


     </head>
     <body>

          <section id="home" class="fill">
               <div class="home-background parallax-section">
                    <div class="container-fluid">
                         <div class="row">
                              <div class="home-box col-xs-12">
                                   <div>
                                        <img src="img/avatar.jpg" alt="">
                                        <h1>Mobarok Hossen</h1>
                                        <p>Laravel Developer / Web Developer</p>
                                   </div>
                                   <div class="social">
                                        <ul>
                                             <li><a href="#"><i class="fa fa-github"></i></a></li>
                                             <li><a href="#"><i class="fa fa-bitbucket"></i></a></li>
                                             <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                             <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                             <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                             <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        </ul>
                                   </div>
                              </div>
                         </div><!-- end row -->
                    </div><!-- end container -->
               </div>
          </section>



          <nav class="main-nav-outer" id="test"><!--main-nav-start-->
               <div class="container-fluid">
                    <ul class="main-nav">
                         <li><a href="#home">Home</a></li>
                         <li><a href="#about">About</a></li>
                         <li><a href="#qualification">Qualification</a></li>
                         <li class="small-logo"><a href="#home"><img src="img/avatar.jpg" alt="" class="img-circle" width="80px"></a></li>
                         <li><a href="#Portfolio">Portfolio</a></li>
                         <li><a href="#contact">Contact</a></li>
                         <li><a href="#team">Blogs</a></li>
                    </ul>
                    <a class="res-nav_click" href="#"><i class="fa-bars"></i></a>
               </div>
          </nav><!--main-nav-end-->



          <!-- About Section
       ================================================== -->
          <section id="about" class="main-section">
               <div class="container">
                    <h2>About Me</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                         eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                         voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                         voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
                         sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                         Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.
                    </p>
                    <div class="row">
                         <div class="columns download">
                              <p>
                                   <a href="#" class="button"><i class="fa fa-download"></i>Download Resume</a>
                              </p>
                         </div>

                    </div> <!-- end row -->

               </div> <!-- end .main-col -->
          </section> <!-- About Section End-->

          <!--skills-->
          <div class="skills main-section">
               <div class="container">
                    <h2>My Skills</h2>
                    <label class="line"></label>
                    <div class="col-md-6 skills-left">
                         <h4>The standard chunk</h4>
                         <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
                         <p>As opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search of lorem ipsum</p> 	
                    </div>
                    <div class="col-md-6 skills-right">
                         <div class="bar_group">
                              <div class='bar_group__bar thin' label='PHP' show_values='true' tooltip='true' value='60'></div>
                              <div class='bar_group__bar thin' label='Laravel' show_values='true' tooltip='true' value='50'></div>
                              <div class='bar_group__bar thin' label='MySQL, Oracle SQL' show_values='true' tooltip='true' value='70'></div>
                              <div class='bar_group__bar thin' label='HTML, CSS, Bootstrap' show_values='true' tooltip='true' value='80'></div>
                              <div class='bar_group__bar thin' label='Javascript, JQuery' show_values='true' tooltip='true' value='55'></div>
                              <div class='bar_group__bar thin' label='Photoshop, Illustrator' show_values='true' tooltip='true' value='65'></div>
                         </div>
                         <script src="js/bars.js"></script>
                    </div>
                    <div class="clearfix"></div>

               </div>
          </div>
          <!--/skills-->
          <!--- Experience Starts Here --->
          <section id="qualification" class="main-section experience">
               <div class="container">
                    <div class="exp-top">
                         <h2 >Qualification</h2>
                         <span class="line"></span>
                    </div>
                    <div class="exp-devide">
                         <h4>Work Experience</h4>
                         <span class="devide-line">
                         </span>
                         <div class="label"><i class="fa fa-briefcase text-primary" aria-hidden="true"></i></div>
                         <div class="exp-devide-grid-right">
                              <h5>UI/UX DESIGNER</h5>
                              <small>Freelancer, 2015</small>
                              <p>Responsible for designing and coding the website. Responsive web design and use slide show. cross browsing compatibility. Duty for testing the code.</p>
                         </div>
                         <div class="exp-devide-grid-left">
                              <h5>Internship (JUNIOR Web Developer)</h5>
                              <small>Workspace Infotech, 2014</small>
                              <p>Building PHP websites using PHP based frameworks. Back end development and maintenance of websites using PHP and MySQL. Developing compatible User Interface functionality using jQuery & other libraries. Developing web sites using MySQL, PHP,  HTML & other programming tools. Ongoing design and maintenance of new and existing websites. Interacting with designers, programmers and clients. Designing websites that are easy and effective to use. Testing websites for functionality in different browsers & at different resolutions. </p>
                         </div>
                    </div>
                    <div class="exp-devide education">
                         <h4>Education</h4>
                         <span class="devide-line2">
                         </span>

                         <div class="label graduation"><i class="fa fa-graduation-cap text-primary" aria-hidden="true"></i></div>
                         <div class="exp-devide-grid-right">
                              <h5>BACHELOR OF SCIENCE</h5>
                              <small>American International University- Bangladesh  2010-2015</small>
                              <p>Computer science and engineering</p>
                         </div>
                         <div class="exp-devide-grid-left">
                              <h5>HIGHER SECONDARY SCHOOL CERTIFICATE </h5>
                              <small>Mern Sun College  2007-2009</small>
                              <p></p>
                         </div>
                         <div class="exp-devide-grid-rightb">
                              <h5>SECONDARY SCHOOL CERTIFICATE </h5>
                              <small>Mirsarai Pilot high School  2007-2009</small>
                              <p></p>
                         </div>
                    </div>
                    <div class="exp-devide training">
                         <h4>Training / Certification</h4>
                         <span class="devide-line3">
                         </span>
                         <div class="label "> <i class="fa fa-book text-primary" aria-hidden="true"></i></div>
                         <div class="exp-devide-grid-right">
                              <h5>BACHELOR OF SCIENCE</h5>
                              <small>American International University- Bangladesh  2010-2015</small>
                              <p>Computer science and engineering</p>
                         </div>
                         <div class="exp-devide-grid-left">
                              <h5>HIGHER SECONDERY SCHOOL CERTIFICATE </h5>
                              <small>Mern Sun College  2007-2009</small>
                              <p></p>
                         </div>
                    </div>
               </div>
          </section>
          <!--- Experience Ends Here --->


          <section class="main-section paddind" id="Portfolio"><!--main-section-start-->
               <div class="container">
                    <h2>Portfolio</h2>
                    <h6>Fresh portfolio of designs that will keep you wanting more.</h6>
                    <div class="portfolioFilter">  
                         <ul class="Portfolio-nav wow fadeIn delay-02s">
                              <li><a href="#" data-filter="*" class="current" >All</a></li>
                              <li><a href="#" data-filter=".branding" >Web Development</a></li>
                              <li><a href="#" data-filter=".webdesign" >Web design</a></li>
                              <li><a href="#" data-filter=".printdesign" >Custom Web Application</a></li>
                              <li><a href="#" data-filter=".photography" >E-Commerce</a></li>
                              <li><a href="#" data-filter=".webdesign" >Responsive Web Design</a></li>
                         </ul>
                    </div> 

               </div>
               <div class="portfolioContainer wow fadeInUp delay-04s">
                    <div class=" Portfolio-box printdesign">
                         <a href="#"><img src="img/Portfolio-pic1.jpg" alt=""></a>	
                         <h3>Foto Album</h3>
                         <p>Print Design</p>
                    </div>
                    <div class="Portfolio-box webdesign">
                         <a href="#"><img src="img/Portfolio-pic2.jpg" alt=""></a>	
                         <h3>Luca Theme</h3>
                         <p>Web Design</p>
                    </div>
                    <div class=" Portfolio-box branding">
                         <a href="#"><img src="img/Portfolio-pic3.jpg" alt=""></a>	
                         <h3>Uni Sans</h3>
                         <p>Branding</p>
                    </div>
                    <div class=" Portfolio-box photography" >
                         <a href="#"><img src="img/Portfolio-pic4.jpg" alt=""></a>	
                         <h3>Vinyl Record</h3>
                         <p>Photography</p>
                    </div>
                    <div class=" Portfolio-box branding">
                         <a href="#"><img src="img/Portfolio-pic5.jpg" alt=""></a>	
                         <h3>Hipster</h3>
                         <p>Branding</p>
                    </div>
                    <div class=" Portfolio-box photography">
                         <a href="#"><img src="img/Portfolio-pic6.jpg" alt=""></a>	
                         <h3>Windmills</h3>
                         <p>Photography</p>
                    </div>
               </div>
          </section><!--main-section-end-->
          <section >
               	<!--- Awards Starts Here ---->
	<div class="awards">
		<div class="container">
			<div class="awards-top">
				<h2>AWARDS & ACHIEVEMENTS</h2>
				<div class="row award-row">
					<div class="col-md-pull-3 award-column">
						<i class="award"></i>
						<h5>10</h5>
						<small>Awards Won!</small>
					</div>
					<div class="col-md-pull-3 award-column">
						<i class="project"></i>
						<h5>15</h5>
						<small>PROJECTS DONE</small>
					</div>
					<div class="col-md-pull-3 award-column">
						<i class="client"></i>
						<h5>6</h5>
						<small>HAPPY CLIENTS</small>
					</div>
					<div class="col-md-pull-3 award-column">
						<i class="cups"></i>
						<h5>580</h5>
						<small>CUPS OF COFFEE</small>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
	</div>
	<!--- Awards Ends Here ---->
          </section>

          <div class="container">
               <section class="main-section contact" id="contact">
                         <h2>Contact Me</h2>
                    <div class="row">
                         <div class="col-lg-6 col-sm-7 wow fadeInLeft">
                              <div class="contact-info-box address clearfix">
                                   <h3><i class=" icon-map-marker"></i>Address:</h3>
                                   <span>308 Negra Arroyo Lane<br>Albuquerque, New Mexico, 87111.</span>
                              </div>
                              <div class="contact-info-box phone clearfix">
                                   <h3><i class="fa-phone"></i>Phone:</h3>
                                   <span>1-800-BOO-YAHH</span>
                              </div>
                              <div class="contact-info-box email clearfix">
                                   <h3><i class="fa-pencil"></i>email:</h3>
                                   <span>hello@knightstudios.com</span>
                              </div>
                              <div class="contact-info-box hours clearfix">
                                   <h3><i class="fa-clock-o"></i>Hours:</h3>
                                   <span><strong>Monday - Thursday:</strong> 10am - 6pm<br><strong>Friday:</strong> People work on Fridays now?<br><strong>Saturday - Sunday:</strong> Best not to ask.</span>
                              </div>
                              <ul class="social-link">
                                   <li class="twitter"><a href="#"><i class="fa-twitter"></i></a></li>
                                   <li class="facebook"><a href="#"><i class="fa-facebook"></i></a></li>
                                   <li class="skype"><a href="#"><i class="fa-skype"></i></a></li>
                                   <li class="gplus"><a href="#"><i class="fa-google-plus"></i></a></li>
                                   <li class="github"><a href="#"><i class="fa-github"></i></a></li>
                                   <li class="bitbucket"><a href="#"><i class="fa-bitbucket"></i></a></li>
                              </ul>
                              <div>
                                   <ul class="hire-link">
                                        <li class="btn btn-primary ">Hire Me <i class="fa fa-arrow-circle-right"></i> </li>
                                        <li ><a href=""><img src="img/PeoplePerHour.jpg" class="img-rounded"></a></li>
                                        <li ><a href=""><img src="img/Guru.jpg" class="img-rounded"></a></li>
                                        <li ><a href=""><img src="img/upwork.png" class="img-rounded"></a></li>
                                        <li ><a href=""><img src="img/freelancer.png" class="img-rounded"></a></li>
                                        <li ><a href=""><img src="img/fiverr.png" class="img-rounded"></a></li>
                                        <li ><a href=""><img src="img/truelancer.png" class="img-rounded"></a></li>
                                   </ul>
                              </div>
                         </div>
                         <div class="col-lg-6 col-sm-5 wow fadeInUp delay-05s">
                              <div class="form">
                                   <input class="input-text" type="text" name="" value="Your Name *" onfocus="if (this.value == this.defaultValue)
                                                  this.value = '';" onblur="if (this.value == '')
                                                                 this.value = this.defaultValue;">
                                   <input class="input-text" type="email" name="" value="Your E-mail *" onfocus="if (this.value == this.defaultValue)
                                                  this.value = '';" onblur="if (this.value == '')
                                                                 this.value = this.defaultValue;">
                                   <input class="input-text" type="text" name="" value="Subject *" onfocus="if (this.value == this.defaultValue)
                                                  this.value = '';" onblur="if (this.value == '')
                                                                 this.value = this.defaultValue;">
                                   <textarea class="input-text text-area" cols="0" rows="0" onfocus="if (this.value == this.defaultValue)
                                                  this.value = '';" onblur="if (this.value == '')
                                                                 this.value = this.defaultValue;">Your Message *</textarea>
                                   <input class="input-btn" type="submit" value="send message">
                              </div>	
                         </div>
                    </div>
               </section>
          </div>
          <footer class="footer">
               <div class="container">
                    <span class="copyright">Copyright © 2016 | By Mobarok Hossen</span>
               </div>
          </footer>


          <script type="text/javascript">
               $(document).ready(function (e) {
                    $('#test').scrollToFixed();
                    $('.res-nav_click').click(function () {
                         $('.main-nav').slideToggle();
                         return false

                    });

               });
          </script>

          <script>
               wow = new WOW(
                       {
                            animateClass: 'animated',
                            offset: 100
                       }
               );
               wow.init();

          </script>


          <script type="text/javascript">
               $(window).load(function () {

                    $('.main-nav li a').bind('click', function (event) {
                         var $anchor = $(this);

                         $('html, body').stop().animate({
                              scrollTop: $($anchor.attr('href')).offset().top - 102
                         }, 1500, 'easeInOutExpo');
                         /*
                          if you don't want to use the easing effects:
                          $('html, body').stop().animate({
                          scrollTop: $($anchor.attr('href')).offset().top
                          }, 1000);
                          */
                         event.preventDefault();
                    });
               })
          </script>

          <script type="text/javascript">

               $(window).load(function () {


                    var $container = $('.portfolioContainer'),
                            $body = $('body'),
                            colW = 375,
                            columns = null;


                    $container.isotope({
                         // disable window resizing
                         resizable: true,
                         masonry: {
                              columnWidth: colW
                         }
                    });

                    $(window).smartresize(function () {
                         // check if columns has changed
                         var currentColumns = Math.floor(($body.width() - 30) / colW);
                         if (currentColumns !== columns) {
                              // set new column count
                              columns = currentColumns;
                              // apply width to container manually, then trigger relayout
                              $container.width(columns * colW)
                                      .isotope('reLayout');
                         }

                    }).smartresize(); // trigger resize to set container width
                    $('.portfolioFilter a').click(function () {
                         $('.portfolioFilter .current').removeClass('current');
                         $(this).addClass('current');

                         var selector = $(this).attr('data-filter');
                         $container.isotope({
                              filter: selector,
                         });
                         return false;
                    });

               });

          </script>
     </body>
</html><?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

